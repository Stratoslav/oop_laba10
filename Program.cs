﻿using System;
using System.Collections.Generic;

namespace SimpleClassLibrary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, Entrant> entrantDictionary = new Dictionary<string, Entrant>();

          
            Entrant entrant1 = new Entrant("Петро Іванов", "ID123", 4.8, true, new ZNO[] { new ZNO("Математика", 170) });
            Entrant entrant2 = new Entrant("Марія Петренко", "ID456", 5.0, true, new ZNO[] { new ZNO("Українська мова", 180) });

            entrantDictionary.Add("ID123", entrant1);
            entrantDictionary.Add("ID456", entrant2);

          
            string searchId = "ID123";
            if (entrantDictionary.ContainsKey(searchId))
            {
                Entrant foundEntrant = entrantDictionary[searchId];
                Console.WriteLine($"Знайдено абітурієнта: {foundEntrant}");
            }
            else
            {
                Console.WriteLine($"Абітурієнта з ID {searchId} не знайдено.");
            }


            foreach (var entrant in entrantDictionary.Values)
            {
                Console.WriteLine(entrant);
            }

            Console.ReadLine();
        }
    }
}
