﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main()
    {

        List<object> mixedList = new List<object>();


        mixedList.Add("Hello");
        mixedList.Add(3.14);
        mixedList.Add(42);
        mixedList.Add(123.45);
        mixedList.Add("World");


        Console.WriteLine("Значення у списку:");

        foreach (var item in mixedList)
        {
         
            if (item is string)
            {
                Console.WriteLine($"Рядок: {item}");
            }
            else if (item is double)
            {
                Console.WriteLine($"Десяткове число: {item}");
            }
            else if (item is int)
            {
                Console.WriteLine($"Ціле число: {item}");
            }
            else
            {
                Console.WriteLine("Інший тип значення");
            }
        }

   
        Console.WriteLine($"Кількість елементів у списку: {mixedList.Count}");

        string searchString = "Hello";
        if (mixedList.Contains(searchString))
        {
            Console.WriteLine($"Елемент '{searchString}' знайдено у списку.");
        }
        else
        {
            Console.WriteLine($"Елемент '{searchString}' не знайдено у списку.");
        }


        mixedList.Remove(42);

       
        Console.WriteLine("\nОновлене значення у списку після вилучення елементу:");

        foreach (var item in mixedList)
        {
            Console.WriteLine(item);
        }

     
        mixedList.Clear();
        Console.WriteLine("\nСписок очищено.");

        Console.ReadLine();
    }
}
